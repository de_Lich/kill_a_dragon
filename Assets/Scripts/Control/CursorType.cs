﻿
namespace RPG.Control
{
    public enum CursorType
    {
        None,
        Movement,
        Combat,
        UI,
        Component,
        Pickup
    }
}
