﻿using PRG.UI.DamageTextUI;
using RPG.Attributes;
using UnityEngine;

namespace RPG.UI.DamageTextUI
{
    public class DamageTextSpawner : MonoBehaviour
    {
        [SerializeField] DamageText damageTextPrefab = null;
        Health health;

        private void Awake()
        {
            health= GetComponent<Health>();
        }
        private void OnEnable()
        {

        }
        public void Spawn (float damageAmount)
        {
            DamageText instance = Instantiate<DamageText>(damageTextPrefab, transform);
            instance.SetValue(damageAmount);
        }
    }
}
