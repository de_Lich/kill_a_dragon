using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class ShowHideUI : MonoBehaviour
    {
        [SerializeField] GameObject uiContainer = null;
        [SerializeField] KeyCode key = KeyCode.Escape;
        void Update()
        {
            if (Input.GetKeyDown(key))
            {
                uiContainer.SetActive(!uiContainer.activeSelf);
            }
        }
    }
}
