﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Inventories
{
    public interface IDragContainer<T>: IDragDestination<T>,IDragSource<T> where T:class
    {
    }
}
