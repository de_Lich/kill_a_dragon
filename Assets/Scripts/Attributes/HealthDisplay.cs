﻿using UnityEngine;
using System;
using UnityEngine.UI;
using RPG.Stats;

namespace RPG.Attributes
{
    public class HealthDisplay : MonoBehaviour
    {
        Health health;
        [SerializeField] Text healthTxt = null;
        void Awake()
        {
            health = GameObject.FindWithTag("Player").GetComponent<Health>();
        }

        void Update()
        {
            healthTxt.text = String.Format("Health: {0:0}/{1:0}",health.GetHealthPoints(),health.GetMaxHealthPoints());
        }
    }
}
