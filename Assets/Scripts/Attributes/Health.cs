﻿using RPG.Core;
using RPG.Saving;
using RPG.Stats;
using UnityEngine;
using GameDevTV.Utils;
using UnityEngine.Events;
using System;

namespace RPG.Attributes
{
    public class Health : MonoBehaviour, ISaveable
    {
        [SerializeField] float regenerationPercentage = 70f;
        [SerializeField] TakeDamageEvent takeDamage;
        [SerializeField] UnityEvent onDie;

        [System.Serializable]
        public class TakeDamageEvent: UnityEvent<float>
        {

        }
        LazyValue<float> healthPoints;
        public float HealthPoints
        {
            get { return healthPoints.Value; }
        }

        BaseStats baseStats;
        [SerializeField] private bool isDead = false;
        public bool IsDead { get; set; }

        private void Awake()
        {
            baseStats = GetComponent<BaseStats>();
            healthPoints = new LazyValue<float>(GetInitialHealth);
        }

        private float GetInitialHealth()
        {
            return GetComponent<BaseStats>().GetStat(Stat.Health);
        }
        private void Start()
        {
            healthPoints.ForceInit();
        }
        private void OnEnable()
        {
            baseStats.onLevelUp += RegenerateHealth;
            
        }
        private void OnDisable()
        {
            baseStats.onLevelUp -= RegenerateHealth;
        }
        public void TakeDamage(GameObject instigator, float damage)
        {
            healthPoints.Value = Mathf.Max(healthPoints.Value - damage, 0);

            if(healthPoints.Value <= 0)
            {
                onDie.Invoke();
                Die();
                AwardExperience(instigator);
            }
            else
            {
                takeDamage.Invoke(damage);
            }
        }
        public void Heal(float healtoRestore)
        {
            healthPoints.Value = Mathf.Min(healthPoints.Value + healtoRestore, GetMaxHealthPoints());
        }
        public float GetHealthPoints()
        {
            return healthPoints.Value;
        }
        public float GetMaxHealthPoints()
        {
            return GetComponent<BaseStats>().GetStat(Stat.Health);
        }
        public float GetPercentage()
        {
            return 100 * GetFraction();
        }
        public float GetFraction()
        {
            return healthPoints.Value / GetComponent<BaseStats>().GetStat(Stat.Health);
        }
        private void Die()
        {
            if (isDead) return;
            IsDead = true;
            CorrectDie();
            GetComponent<Animator>().SetTrigger("die");
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void CorrectDie()
        {
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
            Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
            rigidbody.isKinematic = true;
            rigidbody.useGravity = true;
            
        }

        private void AwardExperience(GameObject instigator)
        {
            Experience experience = instigator.GetComponent<Experience>();
            if (experience == null) return;

            experience.GainExperience(GetComponent<BaseStats>().GetStat(Stat.ExperienceReward));
        }
        private void RegenerateHealth()
        {
            float regenHealthPoints = GetComponent<BaseStats>().GetStat(Stat.Health) * (regenerationPercentage / 100);
            healthPoints.Value = Mathf.Max(healthPoints.Value, regenHealthPoints);
        }

        public object CaptureState()
        {
            return healthPoints.Value;
        }

        public void RestoreState(object state)
        {
            healthPoints.Value = (float)state;
            if (healthPoints.Value <= 0)
            {
                Die();
            }
        }
    }
}
