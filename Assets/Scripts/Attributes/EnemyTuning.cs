﻿using UnityEngine;

namespace RPG.Attributes
{
    public class EnemyTuning : MonoBehaviour
    {
        [SerializeField] Light pointLight = null;

        public void TurnOnLight()
        {
            pointLight.gameObject.SetActive(true);
        }
        public void TurnOffLight() 
        { 
            pointLight.gameObject.SetActive(false); 
        }
    }
}
