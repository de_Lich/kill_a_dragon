﻿using RPG.Control;
using RPG.Core;
using RPG.SceneManagement;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace PRG.SceneManagement
{
    enum DestinationIdentifier
    {
        A,B,C,D,E,F,G
    }
    public class Portal : MonoBehaviour
    {
        [SerializeField] DestinationIdentifier DestinationPoint;
        [SerializeField] int sceneLoaded = -1;
        [SerializeField] GameObject spawnPoint;
        [SerializeField] float fadeOutTime = 0.8f;
        [SerializeField] float fadeInTime = 1.2f;
        [SerializeField] float fadeWaitTime = 0.2f;

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                StartCoroutine(Transition());
            }
        }
        
        private IEnumerator Transition()
        {
            if(sceneLoaded < 0)
            {
                Debug.LogError("Scene to load not set.");
                yield break;
            }
            DontDestroyOnLoad(gameObject);

            Fader fader = FindObjectOfType<Fader>();
            PlayerController playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            playerController.enabled = false;

            yield return fader.FadeOut(fadeOutTime);

            SavingWrapper wrapper = FindObjectOfType<SavingWrapper>();
            wrapper.Save();

            yield return SceneManager.LoadSceneAsync(sceneLoaded);
            PlayerController newPlayerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            newPlayerController.enabled = false;
            wrapper.Load();

            Portal otherPortal = GetOtherPortal();
            UpdatePlayer(otherPortal);
            wrapper.Save();

            yield return new WaitForSeconds(fadeWaitTime);
            fader.FadeIn(fadeInTime);

            newPlayerController.enabled = true;
            Destroy(gameObject);
        }

        private Portal GetOtherPortal()
        {
            foreach (Portal portal in FindObjectsOfType<Portal>())
            {
                if (portal == this) continue;

                if (portal.DestinationPoint != DestinationPoint) continue;
                Debug.Log($"Portal DP is {DestinationPoint} other portal DP is {portal.DestinationPoint}");
                return portal;

            }
            return null;
        }

        private void UpdatePlayer(Portal portal)
        {
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<NavMeshAgent>().enabled = false;
            player.transform.SetPositionAndRotation(portal.spawnPoint.transform.position, portal.spawnPoint.transform.rotation);
            player.GetComponent<NavMeshAgent>().enabled = true;
        }
    }
}
