﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Stats
{
    public class ExperienceDisplay : MonoBehaviour
    {
        Experience experience;
        [SerializeField] Text expText = null;
        void Start()
        {
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
        }

        void Update()
        {
            expText.text = String.Format($"Exp: {experience.ExperiencePoints}");
        }
    }
}
