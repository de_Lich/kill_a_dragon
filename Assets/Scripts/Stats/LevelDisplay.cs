﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Stats
{
    public class LevelDisplay : MonoBehaviour
    {
        BaseStats baseStats;
        [SerializeField] Text levelText = null;
        void Start()
        {
            baseStats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
        }

        void Update()
        {
            levelText.text = String.Format($"Level: {baseStats.CalculateLevel()}");
        }
    }
}