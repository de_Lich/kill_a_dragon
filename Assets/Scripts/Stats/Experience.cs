﻿using RPG.Saving;
using System;
using UnityEngine;

namespace RPG.Stats
{
    class Experience: MonoBehaviour, ISaveable
    {
        [SerializeField] float experiencePoints = 0;
        public float ExperiencePoints 
        {
            get { return experiencePoints; }
            private set { return; }
        }
        //public delegate void ExperienceGainedDelegate();
        public event Action onExperienceGained;

        public void GainExperience(float experience)
        {
            experiencePoints += experience;
            onExperienceGained();
        }
        public object CaptureState()
        {
            return experiencePoints;
        }

        public void RestoreState(object state)
        {
            experiencePoints = (float)state;
            
        }
    }
}
