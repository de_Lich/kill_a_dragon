using RPG.Inventories;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Inventories
{
    [RequireComponent(typeof(Image))]
    public class InventoryItemIcon : MonoBehaviour
    {
        public void SetItem (InventoryItem item)
        {
            Image iconImage = GetComponent<Image>();
            if(item == null)
            {
                iconImage.enabled = false;
            }
            else
            {
                iconImage.enabled = true;
                iconImage.sprite = item.GetIcon();
            }
        }
        public Sprite GetItem()
        {
            Image iconImage = GetComponent<Image>();
            if (!iconImage.enabled)
            {
                return null;
            }
            return iconImage.sprite;
        }
    }
}
