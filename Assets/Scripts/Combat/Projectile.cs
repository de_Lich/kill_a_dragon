﻿using RPG.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] float speed = 1f;
        [SerializeField] float damage = 0f;
        [SerializeField] bool isHoming = true;
        [SerializeField] float maxLifeTime = 0f;
        [SerializeField] GameObject[] destroyOnHit = null;
        [SerializeField] float lifeAfterImpact = .2f;

        [SerializeField] GameObject hitFX = null;
        [SerializeField] UnityEvent onLaunched;
        Health target = null;
        GameObject instigator = null;

        private void Start()
        {
            transform.LookAt(GetAimLocation());
        }
        void Update()
        {
            if (target == null) return;
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            if (isHoming && !target.IsDead)
            {
                transform.LookAt(GetAimLocation());
            }
        }

        public void SetTarget(Health target,GameObject instigator, float damage)
        {
            this.target = target;
            this.damage = damage;
            this.instigator = instigator;
            Destroy(gameObject, maxLifeTime);
        }

        private Vector3 GetAimLocation()
        {
            CapsuleCollider targetCapsule = target.GetComponent<CapsuleCollider>();
            if (targetCapsule == null)
            {
                return target.transform.position;
            }
            return target.transform.position + Vector3.up * targetCapsule.height / 2;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Health>() != target) return;
            if (target.IsDead) return;
            target.TakeDamage(instigator, damage);
            if (hitFX != null)
            {
                onLaunched.Invoke();
                Instantiate(hitFX, GetAimLocation(), transform.rotation);
            }
            foreach (GameObject toDestroy in destroyOnHit)
            {
                Destroy(toDestroy);
            }
            Destroy(gameObject, lifeAfterImpact);

        }
    }
}
